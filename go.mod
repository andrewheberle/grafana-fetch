module gitlab.com/andrewheberle/grafana-fetch

go 1.16

require (
	github.com/gorilla/mux v1.8.0
	github.com/rs/zerolog v1.24.0
	github.com/satori/go.uuid v1.2.0
	github.com/spf13/cobra v1.2.1
	github.com/spf13/viper v1.8.1
)
