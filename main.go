package main

import (
	"gitlab.com/andrewheberle/grafana-fetch/cmd"
)

func main() {
	cmd.Execute()
}
